@extends('layouts.users')

@section('content')
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <form class="form" method="post" action="/users/search">

                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Perform a new Search..." name="search_term">
                        <span class="input-group-btn">
                            <button class="btn btn-success" type="submit">Search</button>
                        </span>
                    </div>

                </div>

            </form>
        </div>
    </div>



    <div class="row">
        <div class=" col-sm-12 col-md-6">

            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Retrieve Images from Places you have already searched:</h3>
              </div>
              <div class="panel-body" id="locationsDiv">
                  <div class="text-muted"></div>
                  <div class="ui-widget">
                            <input type="text" class="form-control" placeholder="Search location names..." id="locations" >
                    </div>
              </div>
            </div>

        </div>
         <div class=" col-sm-12 col-md-6">
             <div class="panel panel-default">
                <div class="panel-heading">
                <h3 class="panel-title">Retrieve Images by Tag:</h3>
                </div>
                <div class="panel-body" id="tagsDiv">
                    <div class="ui-widget">
                        <input type="text" class="form-control" placeholder="Search tag names..." id="tags" >
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function(){

            $( "#locations").val('');
            $( "#tags").val('');

            var locations = $.get( "/users/getJsonLocations", function( data) {

              $( "#locations" ).autocomplete({
                    source: data,
                    select: function( event, ui ) {
                       window.location.assign('/users/show-media/'+ui.item.label);
                    }
                });
              }).done(function() {

              }).fail(function() {
                alert( "Error loading Locations" );
            });

            var tags = $.get( "/users/getJsonTags", function( data) {
              $( "#tags" ).autocomplete({
                    source: data,
                    select: function( event, ui ) {
                        window.location.assign('/users/show-tag/'+ui.item.label);
                    }
                });
              }).done(function() {

              }).fail(function() {
                alert( "Error loading Tags" );
            });
        });
    </script>
@stop
 
 