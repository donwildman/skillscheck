@extends('layouts.users')

@section('content')
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <form class="form" method="post" action="/users/search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search again..." name="search_term">
                    <span class="input-group-btn">
                        <button class="btn btn-success" type="submit">Search</button>
                    </span>
                </div>
            </form>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="text-right">
                <img src="{{ asset('img/poweredby.png') }}" />
            </div>
        </div>


    </div>


    <div class="row">
        <div class="col-sm-12">
            <div id="resultsDiv">

                <ul class="list-group">
                    <li class="list-group-item active">
                        Foursquare API returned the following places of interest...
                    </li>
                @foreach($results as $result)
                    <a href="/users/get-media/{{ $result['id'] }}" class="list-group-item">{{ $result['name'] }}</a>
                @endforeach
                </ul>

            </div>
        </div>
    </div>

@stop
 
 