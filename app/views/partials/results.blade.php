<div id="gallery">
    @foreach($pictures as $picture)
        <?php
             $tags = '';
         if(count($picture->tags) != 0) {  $tags = implode(',', $picture->tags); }

         ?>
        <a href="{{ $picture->low_res }}">
            <img src="{{ $picture->thumbnail }}" alt="#{{ $tags }}" title="{{ $picture->venue->name }}"  />
        </a>
     @endforeach
</div>

