@extends('layouts.users')

@section('content')

   <div class="row">
       <div class="col-md-12">
           <a href="/users/getToken" class="btn btn-primary">Request a new token</a>
       </div>
   </div>

@stop