@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-6 col-md-offset-3">
            <form class="form" method="POST" action="/users/password-reset">
                {{ Form::hidden('token', $token) }}
                <h2 class="form-signin-heading">Reset your Password:</h2>
                <div class="form-group {{ ($errors->has('email') ? 'has-error' : '') }}">
                    <label class="control-label">Email Address</label>
                    <input type="email" id="email" class="form-control" name="email" placeholder="Email Address" value="{{ Input::old('email') }}" required autofocus>
                    <div class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</div>
                </div>

                <div class="form-group {{ ($errors->has('password') ? 'has-error' : '') }}">
                    <label class="control-label">Password:</label>
                    <input type="password" id="password" name="password" class="form-control" required >
                    <div class="help-block">{{ ($errors->has('password') ? $errors->first('password') : '') }}</div>
                </div>

                <div class="form-group {{ ($errors->has('password_confirmation') ? 'has-error' : '') }}">
                    <label class="control-label">Confirm Password:</label>
                    <input type="password" id="password_confirmation" class="form-control" name="password_confirmation" required >
                    <div class="help-block">{{ ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') }}</div>
                </div>



                <button class="btn btn-lg btn-primary btn-block" type="submit">Reset</button>
            </form>
        </div>
    </div>


@stop