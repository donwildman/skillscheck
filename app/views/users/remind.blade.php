@extends('layouts.default')

@section('content')

    <form class="form-signin" action="{{ action('RemindersController@postRemind') }}" method="POST">
        <h2 class="form-signin-heading">Password Reminder</h2>
        <label for="email" class="sr-only">Email Address</label>
        <input type="email" name="email" class="form-control" placeholder="Email Address">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Send Reminder</button>
    </form>

@stop