@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-6 col-md-offset-3">
            <form class="form" method="POST" action="/register">
                {{ Form::token() }}
                <h2 class="form-signin-heading">Register:</h2>
                <div class="form-group {{ ($errors->has('email') ? 'has-error' : '') }}">
                    <label class="control-label">Email Address</label>
                    <input type="email" id="email" class="form-control" name="email" placeholder="Email Address" value="{{ Input::old('email') }}" required autofocus>
                    <div class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</div>
                </div>

                <div class="form-group {{ ($errors->has('password') ? 'has-error' : '') }}">
                    <label class="control-label">Password:</label>
                    <input type="password" id="password" name="password" class="form-control" required >
                    <div class="help-block">{{ ($errors->has('password') ? $errors->first('password') : '') }}</div>
                </div>

                <div class="form-group {{ ($errors->has('password_confirmation') ? 'has-error' : '') }}">
                    <label class="control-label">Confirm Password:</label>
                    <input type="password" id="password_confirmation" class="form-control" name="password_confirmation" required >
                    <div class="help-block">{{ ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') }}</div>
                </div>

                <div class="form-group {{ ($errors->has('first_name') ? 'has-error' : '') }}">
                    <label class="control-label">First Name</label>
                    <input type="text" id="first_name" class="form-control" placeholder="First Name" name="first_name" value="{{ Input::old('first_name') }}" required >
                    <div class="help-block">{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}</div>
                </div>

                <div class="form-group {{ ($errors->has('last_name') ? 'has-error' : '') }}">
                    <label class="control-label">Last Name</label>
                    <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name" value="{{ Input::old('last_name') }}" required >
                    <div class="help-block">{{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}</div>
                </div>

                <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
            </form>
        </div>
    </div>


@stop