@extends('layouts.default')

@section('content')

    <form class="form-signin" method="POST" action="/login">
        {{ Form::token() }}
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="email" class="sr-only">Email Address</label>
        <input type="email" id="email" class="form-control" placeholder="Email address" name="email" required autofocus>
        <label for="password" class="sr-only">Password</label>
        <input type="password" id="password" class="form-control" placeholder="Password" name="password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
    <div class="text-center">
        <a href="/password-reminder">Forgot Password</a>
    </div>
    <hr/>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="text-center">
                <a href="/register" class="btn btn-success ">Register</a>
            </div>
        </div>

    </div>
@stop