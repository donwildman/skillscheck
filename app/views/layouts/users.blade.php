<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SkillsCheck</title>

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('js/ui/jquery-ui.css') }}">
        <link rel="stylesheet" href="{{ asset('js/ui/jquery-ui.theme.css') }}">
        <link rel="stylesheet" href="{{ asset('css/prettyPhoto.css') }}" type="text/css" charset="utf-8" />


        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
        <link href="{{ asset('css/defaults.css') }}" rel="stylesheet">

       <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/ui/jquery-ui.js') }}"></script>
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/users/search"> Welcome {{ Auth::user()->first_name }}</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">

                        <li>
                            <img src="{{ Auth::user()->instagram_profile_picture }}" class="nav_image" height="45" />
                        </li>
                        <li>
                            <a href="/logout"  >Log Out</a>
                        </li>
                    </ul>


                </div><!--/.navbar-collapse -->
            </div>
        </nav>

        

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    @include('layouts/notifications')

                    @yield('content')

                </div>
            </div>

        </div> <!-- /container -->


        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.prettyPhoto.js') }}" type="text/javascript" charset="utf-8"></script>

    </body>
</html>