<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Don Wildman">

        <title>Skills Check Project</title>

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/signin.css') }}" rel="stylesheet">
        
       <script src="{{ asset('js/jquery.min.js') }}"></script>

    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    @include('layouts/notifications')

                    @yield('content')

                </div>
            </div>
        </div>

        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
       
    </body>
</html>