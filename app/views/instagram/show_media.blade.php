@extends('layouts.users')

@section('content')
    <div class="row">

        <div class="col-xs-12 text-center">
            <h3>Images @if(!empty($tag)) tagged with {{ $tag }}@else taken close to {{ $location }}@endif</h3>
        </div>

    </div>


     <div class="row">
         <div class="col-xs-10 col-xs-offset-1">

             <div id="gallery">
                @foreach($pictures as $picture)
                    <?php
                         $tags = '';
                     foreach($picture->tags as $tag)
                     {
                         $tags .= ' #'.$tag->name;
                     }

                     ?>
                 <a href="{{ $picture->original }}" class="img-thumbnail" rel="prettyPhoto[pp_gal]" title="{{ $tags }}">
                     <img src="{{ $picture->thumbnail }}" alt="{{ $picture->caption }}" />
                 </a>

                 @endforeach
            </div>
        </div>
     </div>

    <div class="row">
        <div class="col-sm-6 col-xs-12text-left">
            {{ $pictures->links() }}
        </div>
        <div class="col-sm-6 col-xs-12 m-t-20 text-right">
            <a href="/users/search" class="btn btn-default"><i class="fa fa-arrow-circle-o-left"></i> Back to Search</a>
        </div>
    </div>



<script>
    $(document).ready(function(){
        $("a[rel^='prettyPhoto']").prettyPhoto({social_tools:false});
    });
</script>
@stop