<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pictures', function($table)
		{
			$table->string('id',50);
			$table->string('venue_id',50);
			$table->string('link', 255);
			$table->string('caption',255)->nullable();
			$table->string('type', 25);
			$table->string('low_res', 255);
			$table->string('thumbnail', 255);
			$table->string('original', 255);

			$table->primary('id');

			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pictures');
	}

}
