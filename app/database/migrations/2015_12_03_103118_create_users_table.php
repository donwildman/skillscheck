<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->string('id',50);
			$table->string('email',100);
			$table->string('password',100);
			$table->string('first_name',255);
			$table->string('last_name',255);
			$table->string('instagram_username',100)->nullable();
			$table->string('instagram_access_token',100)->nullable();
			$table->string('instagram_profile_picture',255)->nullable();
			$table->rememberToken();

			$table->primary('id');
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
