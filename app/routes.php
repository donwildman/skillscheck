<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

	Route::get('/', function()
	{
		if(Auth::check())
		{
			// If user has not been issued an instagram token
			if(Auth::user()->instagram_access_token == '') {
					return Redirect::to('/users/getToken');
			}
			return Redirect::to('/users/search');
		} else {
			return View::make('users.login');
		}

	});


	Route::get('/login', array('as' => 'login', 'uses' => 'UsersController@getLogin'));
	Route::post('/login', array('as' => 'post.login', 'uses' => 'UsersController@postLogin'));

	Route::get('/password-reminder', array('as' => 'reminder', 'uses' => 'RemindersController@getRemind'));
	Route::post('/password-reminder', array('as' => 'post.reminder', 'uses' => 'RemindersController@postRemind'));

	Route::get('/password-reset', array('as' => 'reset', 'uses' => 'RemindersController@getReset'));
	Route::post('/password-reset', array('as' => 'post.reset', 'uses' => 'RemindersController@postReset'));

	Route::get('/logout', array('as' => 'logout', 'uses' => 'UsersController@getLogout'));

	Route::get('/register', 'UsersController@getRegister');
	Route::post('/register', 'UsersController@postRegister');

Route::group(array('before' => 'auth','prefix' => 'users'), function() {

	Route::get('authorize', array('as' => 'authorize', 'uses' => 'UsersController@getAuthorize'));
	Route::get('getToken', array('as' => 'tokenize', 'uses' => 'UsersController@getToken'));
	Route::get('manual-token', array('as' => 'no-token', 'uses' => 'UsersController@getManualToken'));

	Route::get('getJsonTags', 'UsersController@getJsonTags');
	Route::get('getJsonLocations', 'UsersController@getJsonLocations');

	Route::get('getImages/{name}', 'UsersController@getImages');

	Route::get('search', 'FoursquareController@getSearch');
	Route::post('search', 'FoursquareController@postSearch');

	Route::get('get-media/{id}', 'UsersController@getMedia');
	Route::get('show-media/{name}', 'UsersController@showMedia');
	Route::get('show-tag/{name}', 'UsersController@showTag');


});