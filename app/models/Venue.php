<?php



class Venue extends Eloquent 
{
	
	protected $table = 'venues';
	
	protected $fillable = array('id', 'name', 'lat','lon','cc');
	
	public function pictures()
	{
		return $this->hasMany('Picture');
	}

	public function users()
	{
		return $this->belongsToMany('User');
	}
}
