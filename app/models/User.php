<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $fillable = array('id', 'username','profile_picture', 'full_name');

	public static $rules = [
		'email' => 'required|email|unique:users',
        'password' => 'required|confirmed|min:6',
        'password_confirmation' => 'required',
        'first_name' => 'required',
        'last_name' => 'required'
	];

	public function venues()
	{
		return $this->belongsToMany('Venue');
	}


}
