<?php



class Location extends Eloquent
{
	
	protected $table = 'locations';
	
	protected $fillable = array('id', 'name', 'latitude','longitude');
	


}
