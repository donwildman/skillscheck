<?php



class Tag extends Eloquent
{
	
	protected $table = 'tags';
	public $timestamps = false;
	
	protected $fillable = array('picture_id','name');
	
	public function pictures()
	{
		return $this->belongsToMany('Picture');
	}

}
