<?php



class Picture extends Eloquent 
{
	
	protected $table = 'pictures';
	
	protected $fillable = array('id','tags','location','venue_id', 'type', 'low_res','thumbnail','original');
	
	public function venue()
	{
		return $this->belongsTo('Venue');
	}

	public function tags()
	{
		return $this->belongsToMany('Tag');
	}

}
