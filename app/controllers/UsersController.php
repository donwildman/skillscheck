<?php


class UsersController extends BaseController {
	


	public function __construct()
	{

	}

    public function getRegister()
    {
        return View::make('users.register');
    }

    public function postRegister()
    {
        $input = [
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'password_confirmation' => Input::get('password_confirmation'),
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
        ];

        $validator = Validator::make($input, User::$rules);

        if ($validator->fails())
        {
            Session::flash('error','Some fields were empty or invalid! Please check your input and try again!');
            return Redirect::to('/register')->withErrors($validator)->withInput();
        } else {

            $user = new User();
            $user->email = $input['email'];
            $user->password = Hash::make($input['password']);
            $user->first_name = $input['first_name'];
            $user->last_name = $input['last_name'];
            if($user->save())
            {
                 Session::flash('success','Your profile has been created. Please log in using the credentials you supplied.');
                return Redirect::to('/login');
            } else {
                Session::flash('error','We could not create your profile! Please check your input and try again');
                return Redirect::to('/register')->withInput();
            }
        }

    }

    public function getLogin()
    {
        return View::make('users.login');
    }

    public function postLogin()
    {

        $input = Input::except(['_token']);
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails())
        {
            Session::flash('error','Please check your credentials and try again...');
            return Redirect::to('/login')->withErrors($validator);
        }else {

            if (Auth::attempt(array('email' => $input['email'], 'password' => $input['password']), true))
            {
                if(Auth::user()->instagram_access_token == '')
                {
                    return Redirect::to('/users/getToken');
                } else {

                    Session::put('instagram_access_token', Auth::user()->instagram_access_token);
                     return Redirect::to('/users/search');
                }
            } else {
                Session::flash('error','Invalid credentials, please try again...');
                return Redirect::to('/login');
            }
        }



    }

    public function getToken()
    {
        Session::forget('instagram_access_token');
        $instagram = OAuth::consumer( 'Instagram', 'http://skillscheck.local/users/authorize' );

        $url = $instagram->getAuthorizationUri();
        return Redirect::to( (string)$url );
    }

    public function getManualToken()
    {
        return View::make('users.manual_token');
    }

    public function getAuthorize()
    {
        Log::info('getAuthorize...');
        $instagram = OAuth::consumer( 'Instagram' );
        $code = Input::get( 'code' );
        try
        {
            $token = $instagram->requestAccessToken( $code );

            $user = $token->getExtraParams();

            $token_string = $token->getAccessToken();

            Session::put('instagram_access_token', $token_string);

            $result = json_decode( $instagram->request( '/users/self' ), true );

            $save_user = Auth::user();
            $save_user->id = $user['user']['id'];
            $save_user->instagram_access_token = $token_string;
            $save_user->instagram_username = $user['user']['username'];
            $save_user->instagram_profile_picture = $user['user']['profile_picture'];
            $save_user->save();
			Auth::login($save_user);
            return Redirect::to('/users/search');

        }
        catch (\OAuth\Common\Http\Exception\TokenResponseException $e)
        {
            Session::flash('error','We could not get a token for you, please try again...');
            return Redirect::to('/users/manual-token');
        }


       
        
    }
	
	public function getLogout()
	{
		Session::forget('instagram_access_token');
		Auth::logout();
		return Redirect::to('/');
	}

    public function getMedia($id)
	{

		$venue = Venue::find($id);
		if(empty($venue))
		{
			Session::flash('error','No Location with that ID');
			return Redirect::to('/users/search');
		}

		$venue->users()->attach(Auth::id());

		$instagram = OAuth::consumer( 'Instagram' );

		$url = '/media/search?lat='.$venue->lat.'&lng='.$venue->lon.'&distance=2000';

		try {
			$results = json_decode($instagram->request($url), true);
		} catch(\OAuth\Common\Storage\Exception\TokenNotFoundException $e){
			// If the token is not found, redirect user to get it again
			 Session::flash('error','Your token has expired, please log in to Instagram to generate another one.');
            return Redirect::to('/users/manual-token');
		}


		foreach($results['data'] as $photo)
		{

				$picture = Picture::firstOrNew(['id' => $photo['id']]);
				$picture->venue_id = $venue->id;
				$picture->type = $photo['type'];
//				$picture->location = $photo['location']['name'];
				$picture->link = $photo['link'];
				$picture->caption = $photo['caption'];
				$picture->low_res = $photo['images']['low_resolution']['url'];
				$picture->thumbnail = $photo['images']['thumbnail']['url'];
				$picture->original = $photo['images']['standard_resolution']['url'];
				if($picture->save())
				{
					foreach($photo['tags'] as $tagname)
					{
						Log::info($tagname);
						$tag = Tag::firstOrCreate(['name'=>$tagname]);
						$picture->tags()->assign($tag->id);
					}
				}

		}

		return Redirect::to('/users/show-media/'.$venue->name);
	}

    public function showMedia($name)
    {
        $venue = Venue::with('pictures.tags')->where('name',$name)->first();
        if (empty($venue)) {
            Session::flash('error', 'No Venue with that ID');
            return Redirect::to('/users/search');
        }
        $data['pictures'] = $venue->pictures()->paginate(10);
        $data['location'] = $name;

        return View::make('instagram.show_media', $data);
    }

	public function showTag($name)
    {
        $tag = Tag::with('pictures')->where('name',$name)->first();
        if (empty($tag)) {
            Session::flash('error', 'That Tag doesn\'t exist!');
            return Redirect::to('/users/search');
        }
        $data['pictures'] = $tag->pictures()->paginate(10);
        $data['tag'] = $name;

        return View::make('instagram.show_media', $data);
    }

    public function getJsonTags()
    {
	    $tags = Tag::orderBy('name')->distinct()->lists('name');
		return Response::json($tags);
    }

	public function getJsonLocations()
	{
		$user = User::with('venues')->find(Auth::user()->id);
		$locations = $user->venues->lists('name');
		return Response::json($locations);
	}
}
