<?php
	
	use Jcroll\FoursquareApiClient\Client\FoursquareClient;
	
class FoursquareController extends BaseController {
	
	public function getSearch()
	{
		$user = User::find(Auth::user()->id);
		return View::make('foursquare.search');
	}
	
	public function postSearch()
	{
		$location = Input::get('search_term');

		$client = FoursquareClient::factory(array(
				'client_id' => 'YAJGN0TSRILPKHPHZKXS4E3P4M1UKMSH1OOHB3D4IKJLLPZN',
				'client_secret' => '2BIANV4NI5ASMZ05RELDI5XI54NWYERRNCJVICPDE33LD3RP'
		));
		//$client->addToken($oauthToken); // optionally pass in for user specific requests
		$command = $client->getCommand('venues/search', array('near' => $location));

		try {
			$results = $command->execute();

			$data['results'] = [];
			foreach($results['response']['venues'] as $venue)
			{
				$params = [
					'id' => $venue['id'],
					'name' => $venue['name'],
					'lat' => $venue['location']['lat'],
					'lon' => $venue['location']['lng'],
					'cc' => $venue['location']['cc']
				];
				$venue = Venue::firstOrCreate($params);
				$data['results'][] = $params;
			}
			return View::make('foursquare.results', $data);
		}
		catch (\Guzzle\Http\Exception\ClientErrorResponseException $e)
		{
			Session::flash('error','Foursquare didn\'t like that, please try again...');
            return Redirect::to('/users/search');
		}




	}

}
